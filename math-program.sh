# Author Sterneus
# ID:1604789
# this script accepts three numbers and performs some calculations then writes them to a file

#!/usr/bin/env bash


 NUM1=#1
 NUM2=#2
 NUM3=#3

 echo "the first number is : "
 echo $NUM1
 echo "the second number is:  "
 echo $NUM2
 echo "the third number is :  "
 echo $NUM3

if [ -z "${OUTPUT_FILE_NAME}" ] # Returns True if the variable is NOT already declared
then
    # Define variable with default value
    OUTPUT_FILE_NAME="myresult.txt"
    echo "Using default output file name of '$OUTPUT_FILE_NAME'."
    echo "Do you want to change the file name ---- Enter y for YES and n for NO"

read -r  answer
#Yes="y"
if [[ $answer = "y" ]]; then
echo "Enter the new filename"
read -r newfilename
OUTPUT_FILE_NAME=$newfilename
fi

    echo "Using given file name of '$OUTPUT_FILE_NAME'"
fi


function calculation(){
res1=$((NUM1 ** NUM2))
res2=$((NUM3 * NUM2))
finalresult=$((res1 + res2))

echo " The following calculation will be carried out ($NUM1 ^ $NUM2) + $NUM3 x $NUM2 = $finalresult"

sleep 2s

echo "The result is: "
echo $finalresult


echo $finalresult  >> "$OUTPUT_FILE_NAME"

sleep 10s

}

calculation

 
 #program closes too quickly  revise code below
echo "Exiting Program"
